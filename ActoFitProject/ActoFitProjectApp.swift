//
//  ActoFitProjectApp.swift
//  ActoFitProject
//
//  Created by JM MAC MINI 4 on 19/12/22.
//

import SwiftUI

@main
struct ActoFitProjectApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
//            ContentView()
//                .environment(\.managedObjectContext, persistenceController.container.viewContext)
            DashBoardView().environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
