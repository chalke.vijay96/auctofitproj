//
//  MeasurementTableViewCell.swift
//  ActoFitProject
//
//  Created by JM MAC MINI 4 on 20/12/22.
//

import UIKit

protocol MeasurementTableViewCellDelegate {
    func textFieldClicked(measureUnits: MeasureUnits?, valueStr: String?)
}

class MeasurementTableViewCell: UITableViewCell {

    @IBOutlet weak var txt_Value: UITextField!
    @IBOutlet weak var lbl_UnitName: UILabel!
    @IBOutlet weak var imgVIew: UIImageView!
    var paramDict : [String:String] = [:]
    var delegate : MeasurementTableViewCellDelegate!
    var vc : UIViewController?
    var itemPicker: UIPickerView! = UIPickerView()
    var numArr : [Double] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var measureUnitVal: MeasureUnits? {

        didSet {

            if let unitVal = measureUnitVal{
                self.imgVIew.image = UIImage.init(named: unitVal.bodyPartImg)
                self.lbl_UnitName.text = unitVal.bodyPartStr
                self.txt_Value.delegate = self
                self.txt_Value.inputView = itemPicker
                self.itemPicker.delegate = self
                self.txt_Value.attributedPlaceholder = NSAttributedString(
                    string: "Range between \(unitVal.rangeA) to \(unitVal.rangeB) Cm"
                )
                if let unitVal = self.paramDict[unitVal.bodyPartStr]{
                    self.txt_Value.text = unitVal
                }else {
                    self.txt_Value.text = ""
                }
//                let rangeA = Int(unitVal.rangeA)
//                let rangeB = Int(unitVal.rangeB)
//                for i in rangeA..<rangeB+1 {
//                    numArr.append(Double(i))
//                }
                self.dismissPickerView()
            }
        }
    }
    
    func dismissPickerView() {
       let toolBar = UIToolbar()
       toolBar.sizeToFit()
       let button = UIBarButtonItem(title: "Done" , style: .plain, target: self, action: #selector(btnClick(sender:)))
       toolBar.setItems([button], animated: true)
       toolBar.isUserInteractionEnabled = true
       txt_Value.inputAccessoryView = toolBar
    }
    
    @objc func btnClick(sender: UIBarButtonItem) {
        self.vc?.view.endEditing(true)
    }
    
}

extension MeasurementTableViewCell: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
        self.delegate.textFieldClicked(measureUnits: self.measureUnitVal ,valueStr: textField.text!)
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
//        self.delegate.textFieldClicked(measureUnits: measureUnitVal)
        if let unitsVal = measureUnitVal {
            let rangeA = Int(unitsVal.rangeA)
            let rangeB = Int(unitsVal.rangeB)
            for i in rangeA..<rangeB+1 {
                numArr.append(Double(i))
            }
        }
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return false
    }
}

extension MeasurementTableViewCell: UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return numArr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(numArr[row])"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        self.txt_Value.text = "\(numArr[row])"
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        itemPicker.isHidden = false
        return false
    }
}
