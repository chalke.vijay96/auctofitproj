//
//  DashInfoTableViewCell.swift
//  ActoFitProject
//
//  Created by JM MAC MINI 4 on 20/12/22.
//

import UIKit

class DashInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var btn_Delete: UIButton!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var collection_View: UICollectionView!
    var dictInfo : [String:String] = [:]
    var keysArr : [String] = []
    var vc : UIViewController?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCollectionView(){
        self.collection_View.delegate = self
        self.collection_View.dataSource = self
        self.collection_View.register(UINib(nibName: "MeasureTypeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MeasureTypeCollectionViewCell")
        keysArr.removeAll()
        for (key,_) in dictInfo {
            keysArr.append(key)
        }
        self.collection_View.reloadData()
    }
    
    @IBAction func btn_DeleteClicked(_ sender: Any) {
        
    }
}

extension DashInfoTableViewCell :  UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return keysArr.count  //return number of rows in section
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : MeasureTypeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MeasureTypeCollectionViewCell", for: indexPath) as! MeasureTypeCollectionViewCell
        let keyStr = keysArr[indexPath.row]
        cell.lbl_One.text = keyStr as! String
        cell.lbl_Two.text = self.dictInfo[keyStr] as! String
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collection_View.frame.size.width/3.2, height: 100)
    }
    
    
    
}
