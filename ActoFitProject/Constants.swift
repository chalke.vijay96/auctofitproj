//
//  Constants.swift
//  ActoFitProject
//
//  Created by JM MAC MINI 4 on 20/12/22.
//

import Foundation

struct MeasureKeys{
    
    static let Neck = "Neck"
    static let Bicep_Left = "Bicep-Left"
    static let Bicep_Right = "Bicep-Right"
    static let Chest = "Chest"
    static let Forearm_Left = "Forearm-Left"
    static let Forearm_Right = "Forearm-Right"
    static let Waist = "Waist"
    static let Hips = "Hips"
    static let Thigh_Left = "Thigh-Left"
    static let Thigh_Right = "Thigh-Right"
    static let Calf_Left = "Calf-Left"
    static let Calf_Right = "Calf-Right"
    static let Shoulder = "Shoulder"
    static let Abdomen = "Abdomen"
    
}

struct UserDefaultsKey{
    static let Body_Measurements_Info = "Body_Measurements_Info"
}

struct MeasureUnits{
    
    var bodyPartImg : String
    var bodyPartStr : String
    var rangeA : Double
    var rangeB : Double
    
    init(bodyPartImg : String, bodyPartStr : String, rangeA : Double, rangeB : Double) {
        self.bodyPartImg = bodyPartImg
        self.bodyPartStr = bodyPartStr
        self.rangeA = rangeA
        self.rangeB = rangeB
    }
    
}
