//
//  MeasurementVC.swift
//  ActoFitProject
//
//  Created by JM MAC MINI 4 on 19/12/22.
//

import UIKit

protocol MeasurementVCDelegate {
    func reloadUI()
}
class MeasurementVC: UIViewController {

    var delegate : MeasurementVCDelegate!
    @IBOutlet weak var tableViews: UITableView!
    var selectedMeasure : MeasureUnits?
    var paramDict : [String:String] = [:]
    let measureUnitsArr = [MeasureUnits(bodyPartImg: "ic_neck", bodyPartStr: MeasureKeys.Neck, rangeA: 15.0, rangeB: 28.0),
                           MeasureUnits(bodyPartImg: "ic_bicepleft", bodyPartStr: MeasureKeys.Bicep_Left, rangeA: 19.0, rangeB: 76.0),
                           MeasureUnits(bodyPartImg: "ic_bicepright", bodyPartStr: MeasureKeys.Bicep_Right, rangeA: 19.0, rangeB: 76.0),
                           MeasureUnits(bodyPartImg: "ic_chest", bodyPartStr: MeasureKeys.Chest, rangeA: 53.0, rangeB: 210.0),
                           MeasureUnits(bodyPartImg: "ic_forearmleft", bodyPartStr: MeasureKeys.Forearm_Left, rangeA: 15.0, rangeB: 61.0),
                           MeasureUnits(bodyPartImg: "ic_forearmright", bodyPartStr: MeasureKeys.Forearm_Right, rangeA: 15.0, rangeB: 61.0),
                           MeasureUnits(bodyPartImg: "ic_waist", bodyPartStr: MeasureKeys.Waist, rangeA: 36.0, rangeB: 146.0),
                           MeasureUnits(bodyPartImg: "ic_hips", bodyPartStr: MeasureKeys.Hips, rangeA: 45.0, rangeB: 178.0),
                           MeasureUnits(bodyPartImg: "ic_thighleft", bodyPartStr: MeasureKeys.Thigh_Left, rangeA: 25.0, rangeB: 108.0),
                           MeasureUnits(bodyPartImg: "ic_thighright", bodyPartStr: MeasureKeys.Thigh_Right, rangeA: 25.0, rangeB: 108.0),
                           MeasureUnits(bodyPartImg: "ic_thighleft", bodyPartStr: MeasureKeys.Calf_Left, rangeA: 15.0, rangeB: 68.0),
                           MeasureUnits(bodyPartImg: "ic_thighright", bodyPartStr: MeasureKeys.Calf_Right, rangeA: 15.0, rangeB: 68.0),
                           MeasureUnits(bodyPartImg: "ic_shoulder", bodyPartStr: MeasureKeys.Shoulder, rangeA: 19.0, rangeB: 76.0),
                           MeasureUnits(bodyPartImg: "ic_abdomen", bodyPartStr: MeasureKeys.Abdomen, rangeA: 20.0, rangeB: 210.0),
    ]
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableViews.register(UINib(nibName: "MeasurementTableViewCell", bundle: nil), forCellReuseIdentifier: "MeasurementTableViewCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        
        // Do any additional setup after loading the view.
    }

    @objc func keyboardWillShow(_ notification:Notification) {

        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.tableViews.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }
    @objc func keyboardWillHide(_ notification:Notification) {

        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.tableViews.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }
    
    @IBAction func btn_BackClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_SaveClicked(_ sender: Any) {
        if let errorMsg = checkDictValue(){
            let ac = UIAlertController(title: errorMsg, message: nil, preferredStyle: .alert)
                let submitAction = UIAlertAction(title: "Ok", style: .default) { [weak self, weak ac] action in
                }
                ac.addAction(submitAction)
                present(ac, animated: true)
        }else {
            print("SAVE VALUE IN SHARED")
            let date = Date()
            
            let formatDate = date.getFormattedDate(format: "dd-MM-yyyy HH:mm:ss")
            
            let defaults = UserDefaults.standard

            if var dictionary = defaults.value(forKey: UserDefaultsKey.Body_Measurements_Info) as? [String : Any] {
                
                dictionary[formatDate] = self.paramDict
                
                print("SAVED VALUE ONE \(dictionary)")
                defaults.set(dictionary, forKey: UserDefaultsKey.Body_Measurements_Info)
                
            }else {
                var dict : [String:Any] = [:]
                dict[formatDate] = self.paramDict
                print("SAVED VALUE TWO \(dict)")
                defaults.set(dict, forKey: UserDefaultsKey.Body_Measurements_Info)
            }
            self.delegate.reloadUI()
            self.dismiss(animated: true, completion: nil)
            
        }
    }
    
    func checkDictValue() -> String?{
        for unitOBj in measureUnitsArr {
            if let keyVal = self.paramDict[unitOBj.bodyPartStr]{
                if (keyVal == ""){
                    return "Enter Value Of \(keyVal)"
                }
            }else{
                return "Enter Value Of \(unitOBj.bodyPartStr)"
            }
        }
        return nil
    }
}

extension MeasurementVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return measureUnitsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MeasurementTableViewCell", for: indexPath) as! MeasurementTableViewCell
        cell.measureUnitVal = self.measureUnitsArr[indexPath.row]
        cell.delegate = self
        cell.vc = self
        cell.paramDict = paramDict
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    

}

extension MeasurementVC: MeasurementTableViewCellDelegate {
    
    func textFieldClicked(measureUnits: MeasureUnits?, valueStr: String?) {
        if let unit = measureUnits{
            self.paramDict[unit.bodyPartStr] = valueStr
            print("paramDict \(self.paramDict)")
        }
    }
}


// MARK: - UIPickerViewDelegate

//extension MeasurementVC: UIPickerViewDelegate, UIPickerViewDataSource {
//
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
//
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        if let measure = self.selectedMeasure{
//            let range = measure.rangeB - measure.rangeA
//            return Int(range)
//        }
//        return 0
//    }
//
//    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        if let measure = self.selectedMeasure{
//            let range = measure.rangeA + Double(row)
//            return "\(range)"
//        }
//        return ""
//    }
//
//    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//
//        pickerView.isHidden = true
//    }
//}
