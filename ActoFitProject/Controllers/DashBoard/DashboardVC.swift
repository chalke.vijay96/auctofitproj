//
//  DashboardVC.swift
//  ActoFitProject
//
//  Created by JM MAC MINI 4 on 19/12/22.
//

import UIKit
import Foundation
import SwiftUI


struct DashBoardView: View {
    let reference = Reference<WelcomeScreenVC>()
    
    var body: some View {
//        Button("Resume") {
//            reference.object?.resume()
//        }
//        Button("Pause") {
//            reference.object?.pause()
//        }
        PlayerView(reference: reference)
    }
}

struct PlayerView: UIViewControllerRepresentable {
    let reference: Reference<WelcomeScreenVC>
    
    func makeUIViewController(context: Context) -> WelcomeScreenVC {
        let controller = WelcomeScreenVC()
        
        // Set controller to the reference
        reference.object = controller

        return controller
    }
    
    func updateUIViewController(_ viewController: WelcomeScreenVC, context: Context) {
    }
}

class Reference<T: AnyObject> {
    weak var object: T?
}

class WelcomeScreenVC: UIViewController {
    
    override func viewDidLoad() {
        self.view.backgroundColor = .lightGray
        let button:UIButton = UIButton(frame: CGRect(x: 100, y: 100, width: self.view.frame.width-200, height: 55))
        button.backgroundColor = .blue
        button.layer.cornerRadius = 10.0
        button.setTitle("Click To Proceed", for: .normal)
//        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
        self.view.addSubview(button)
        
    }
    
    
    @objc func buttonClicked() {
         print("Button Clicked")
        let vc = DashboardVC(nibName: "DashboardVC", bundle: nil)
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
}


class DashboardVC: UIViewController {

    @IBOutlet weak var tbl_View: UITableView!
    var allDataDict: [String:Any] = [:]
    var keysArr : [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tbl_View.register(UINib(nibName: "DashInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "DashInfoTableViewCell")

        self.reloadAllDATA()
        
        
        // Do any additional setup after loading the view.
    }

    func reloadAllDATA(){
        let defaults = UserDefaults.standard
        if let isDict = defaults.value(forKey: UserDefaultsKey.Body_Measurements_Info) as? [String : Any]{
            self.allDataDict = isDict
            keysArr.removeAll()
            for (key,_) in allDataDict {
                keysArr.append(key)
            }
            self.tbl_View.reloadData()
        }
    }
    
    @IBAction func segmentCicked(_ sender: Any) {
    }
    
    @IBAction func btn_BackClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_AddMeasureClicked(_ sender: Any) {
        let vc = MeasurementVC(nibName: "MeasurementVC", bundle: nil)
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    

}

extension DashboardVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return keysArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DashInfoTableViewCell", for: indexPath) as! DashInfoTableViewCell
        cell.vc = self
        let keyStr = keysArr[indexPath.row]
        cell.lbl_Time.text = keyStr
        if let dict = self.allDataDict[keyStr] as? [String : String]{
            cell.dictInfo = dict
        }
        cell.btn_Delete.tag = indexPath.row
        cell.btn_Delete.addTarget(self, action: #selector(deleted(sender:)), for: .touchUpInside)
        
        cell.setCollectionView()
        return cell
    }
    
    @objc func deleted(sender: UIButton){
        
        let ac = UIAlertController(title: "Are you sure you want to delete", message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { [weak self, weak ac] action in
            if let keyStr = self?.keysArr[sender.tag] {
                if let index = self?.allDataDict.index(forKey: keyStr) {
                    self?.allDataDict.remove(at: index)
                    print("ALL DICT DATA \(self?.allDataDict)")
                }
            }
            let defaults = UserDefaults.standard
            defaults.set(self?.allDataDict, forKey: UserDefaultsKey.Body_Measurements_Info)
            self?.reloadAllDATA()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { [weak self, weak ac] action in
            
        }
        ac.addAction(okAction)
        ac.addAction(cancelAction)
        present(ac, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    

}

extension DashboardVC: MeasurementVCDelegate {
    func reloadUI() {
        self.reloadAllDATA()
    }
}
